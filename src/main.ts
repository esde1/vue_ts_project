import { createApp } from 'vue'
import App from './App.vue'
import { create, NButton, NConfigProvider } from 'naive-ui'

const naive = create({
    components: [NButton, NConfigProvider]
})

createApp(App)
    .use(naive)
    .mount('#app')