
type Profile = {
    uid: string;
    firstName: string;
    lastName: string;
    age: number;
};

const profilesDb: Profile[] = [
    { uid: '1', firstName: 'Sponge', lastName: 'Bob', age: 25 },
    { uid: '2', firstName: 'Patrik', lastName: 'Star', age: 30 },
    { uid: '3', firstName: 'Mister', lastName: 'Crabs', age: 28 },
];

const loadProfiles = (): Promise<Profile[]> => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(profilesDb);
        }, 3000);
    });
};

export { loadProfiles };

